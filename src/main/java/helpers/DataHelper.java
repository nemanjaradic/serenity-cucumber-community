package helpers;

import com.github.javafaker.Faker;
import models.Category;
import models.Pet;
import models.Tag;

import java.util.ArrayList;
import java.util.List;

public class DataHelper {

    Faker faker = new Faker();

    public Pet createNewPet() {

        Pet newPet = new Pet();
        newPet.setId((long) faker.number().numberBetween(1000, 10000));
        newPet.setName(faker.dog().name());
        newPet.setCategory(setPetCategory());
        newPet.setPhotoUrls(setPhotos());
        newPet.setTags(setTags());
        newPet.setStatus(Pet.StatusEnum.AVAILABLE);
        return newPet;
    }

    private Category setPetCategory() {
        Category setCategory = new Category();
        setCategory.setId((long) faker.number().numberBetween(1000, 10000));
        setCategory.setName(faker.dog().breed());
        return setCategory;
    }

    private List<String> setPhotos() {
        List<String> photoUrls = new ArrayList<>();
        photoUrls.add(faker.internet().image());
        return photoUrls;
    }

    private List<Tag> setTags() {
        Tag tag = new Tag();
        tag.setId((long) faker.number().numberBetween(1000, 10000));
        tag.setName(faker.dog().gender());
        List<Tag> tags = new ArrayList<>();
        tags.add(tag);
        return tags;
    }
}
