@Pet
Feature: All about pets

  @TEST-01
  Scenario: Create new pet
    When I create new Pet
    Then I should receive status code 200
    And I should receive appropriate pet details in response

  @Smoke
  @TEST-02
  Scenario: Retrieve previously created pet
    When I create new Pet
    Then I should receive status code 200
    When I retrieve previously created pet by pet id
    Then I should receive appropriate pet details in response

  @TEST-03
  Scenario Outline: Get pet by non existing pet id
    When I retrieve a pet by <pet_id> pet id
    Then I should receive status code <status_code>

    Examples:
      | pet_id    | status_code |
      | TEST12345 | 400         |
      | 123456789 | 404         |

  @TEST-04
  Scenario Outline: Update pet
    Given New pet is created
    When I update newly created pet name <pet_name> and pet status <pet_status>
    Then I should receive status code 200
    And I should receive appropriate pet details in response
    Examples:
      | pet_name | pet_status |
      | Max      | available  |
      | Buddy    | pending    |
      | Simba    | sold       |

  @Smoke
  @TEST-05
  Scenario: Delete pet
    Given New pet is created
    When I delete newly created pet
    Then I should receive status code 200
    And I should receive appropriate delete message in response
