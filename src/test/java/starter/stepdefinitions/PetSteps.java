package starter.stepdefinitions;

import helpers.DataHelper;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import models.Pet;
import net.serenitybdd.core.Serenity;
import org.assertj.core.api.SoftAssertions;
import starter.api.RestApiService;

public class PetSteps {
    RestApiService petStoreApi = new RestApiService();
    DataHelper dataHelper = new DataHelper();
    SoftAssertions softAssert = new SoftAssertions();

    @When("I create new Pet")
    @Given("New pet is created")
    public void createNewPet() {
        Pet petRequestBody = dataHelper.createNewPet();
        petStoreApi.postRequestWithDetails(petRequestBody, "/pet");
    }

    @Then("I should receive appropriate pet details in response")
    public void iShouldReceiveProperDetailsPet() {
        Pet petResponse = RestApiService.then().extract().as(Pet.class);
        Pet petRequest = Serenity.sessionVariableCalled(RestApiService.LAST_REQUEST_BODY);

        softAssert.assertThat(petResponse.getName()).as("Pet Name").isEqualTo(petRequest.getName());
        softAssert.assertThat(petResponse.getId()).as("Pet ID").isEqualTo(petRequest.getId());
        softAssert.assertThat(petResponse.getCategory()).as("Category").isNotNull();
        softAssert.assertThat(petResponse.getCategory().getName()).as("Category Name")
                .isEqualTo(petRequest.getCategory().getName());
        softAssert.assertAll();
    }

    @Then("I should receive status code {int}")
    public void iShouldReceiveStatusCode(Integer statusCode) {
        RestApiService.then().statusCode(statusCode);
    }

    @When("I retrieve previously created pet by pet id")
    public void iRetrievePreviouslyCreatedPetById() {
        Pet petResponse = RestApiService.then().extract().as(Pet.class);
        String petId = String.valueOf(petResponse.getId());
        petStoreApi.get(String.format("%s/%s", "/pet", petId));
    }

    @When("I retrieve a pet by {word} pet id")
    public void iRetrievePetByNonExistingId(String petId) {
        petStoreApi.get(String.format("%s/%s", "/pet", petId));
    }

    @When("I update newly created pet name {word} and pet status {word}")
    public void iUpdatePet(String name, String status) {
        Pet petRequest = Serenity.sessionVariableCalled(RestApiService.LAST_REQUEST_BODY);
        petRequest.setName(name);
        petRequest.setStatus(Pet.StatusEnum.fromValue(status));
        petStoreApi.update(petRequest, "/pet");
    }

    @When("I delete newly created pet")
    public void iDeleteNewlyCreatedPet() {
        Pet petResponse = RestApiService.then().extract().as(Pet.class);
        String petId = String.valueOf(petResponse.getId());
        petStoreApi.delete(String.format("%s/%s", "/pet", petId));
    }

    @Then("I should receive appropriate delete message in response")
    public void iShouldReceiveDeleteMessageSuccess() {
        String petResponse = RestApiService.then().extract().body().asString();
        softAssert.assertThat(petResponse).as("Pet Response Body").isEqualTo("Pet deleted");
        softAssert.assertAll();
    }
}


