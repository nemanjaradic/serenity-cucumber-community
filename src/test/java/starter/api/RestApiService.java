package starter.api;

import io.restassured.response.ValidatableResponse;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.serenitybdd.rest.SerenityRest;

import java.util.Map;

public class RestApiService {
    public static final String LAST_REQUEST_BODY = "lastRequestBody";
    private static final String LAST_RESPONSE = "lastResponse";
    private static final String BASE_URL = "http://localhost:8080/api/v3";

    @Step("Post request with details")
    public ValidatableResponse postRequestWithDetails(Object body, String endPoint) {
        ValidatableResponse response = SerenityRest.given()
                .baseUri(BASE_URL)
                .log().all()
                .contentType("application/json")
                .header("Content-Type", "application/json")
                .body(body)
                .when().log().all()
                .post(endPoint)
                .then();
        Serenity.setSessionVariable(LAST_RESPONSE).to(response);
        Serenity.setSessionVariable(LAST_REQUEST_BODY).to(body);
        return response;
    }

    @Step("Get request")
    public ValidatableResponse get(String endPoint) {
        ValidatableResponse response = SerenityRest.given()
                .baseUri(BASE_URL)
                .log().all()
                .contentType("application/json")
                .header("Content-Type", "application/json")
                .when()
                .get(endPoint)
                .then();
        Serenity.setSessionVariable(LAST_RESPONSE).to(response);
        return response;
    }

    @Step("Get request with path parameter")
    public ValidatableResponse get(String endPoint, Map<String, String> pathParameter) {
        ValidatableResponse response = SerenityRest.given()
                .baseUri(BASE_URL)
                .log().all()
                .contentType("application/json")
                .header("Content-Type", "application/json")
                .pathParams(pathParameter)
                .when()
                .get(endPoint)
                .then();
        Serenity.setSessionVariable(LAST_RESPONSE).to(response);
        return response;
    }

    @Step("Delete request")
    public ValidatableResponse delete(String endPoint) {
        ValidatableResponse response = SerenityRest.given()
                .baseUri(BASE_URL)
                .log().all()
                .contentType("application/json")
                .header("Content-Type", "application/json")
                .when()
                .delete(endPoint)
                .then();
        Serenity.setSessionVariable(LAST_RESPONSE).to(response);
        return response;
    }

    @Step("Update request")
    public ValidatableResponse update(Object body, String endPoint) {
        ValidatableResponse response = SerenityRest.given()
                .baseUri(BASE_URL)
                .log().all()
                .contentType("application/json")
                .header("Content-Type", "application/json")
                .body(body)
                .when()
                .put(endPoint)
                .then();
        Serenity.setSessionVariable(LAST_RESPONSE).to(response);
        Serenity.setSessionVariable(LAST_REQUEST_BODY).to(body);
        return response;
    }

    @Step("Update request with path parameters")
    public ValidatableResponse update(Object body, String endPoint, Map<String, String> pathParameter) {
        ValidatableResponse response = SerenityRest.given()
                .baseUri(BASE_URL)
                .log().all()
                .contentType("application/json")
                .header("Content-Type", "application/json")
                .pathParams(pathParameter)
                .body(body)
                .when()
                .put(endPoint)
                .then();
        Serenity.setSessionVariable(LAST_RESPONSE).to(response);
        Serenity.setSessionVariable(LAST_REQUEST_BODY).to(body);
        return response;
    }

    public static ValidatableResponse then() {
        return Serenity.sessionVariableCalled(LAST_RESPONSE);
    }
}

